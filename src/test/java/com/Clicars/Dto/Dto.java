package com.Clicars.Dto;

public class Dto {

    private static String Brand;
    private static String Price;
    private static String Model;
    private static String fuel;
    private static String Km;
    private static String Transmission;
    private static String Year;

    public Dto(){
        Brand = null;
        Price = null;
        Model = null;
        fuel = null;
        Km = null;
        Transmission = null;
        Year = null;

    }

    public static String getBrand() {
        return Brand;
    }

    public static void setBrand(String brand) {
        Brand = brand;
    }

    public static String getPrice() {
        return Price;
    }

    public static void setPrice(String price) {
        Price = price;
    }

    public static String getModel() {
        return Model;
    }

    public static void setModel(String model) {
        Model = model;
    }

    public static String getFuel() {
        return fuel;
    }

    public static void setFuel(String fuel) {
        Dto.fuel = fuel;
    }

    public static String getKm() {
        return Km;
    }

    public static void setKm(String km) {
        Km = km;
    }

    public static String getTransmission() {
        return Transmission;
    }

    public static void setTransmission(String transmission) {
        Transmission = transmission;
    }

    public static String getYear() {
        return Year;
    }

    public static void setYear(String year) {
        Year = year;
    }
}
