package com.Clicars.Utils;

public class DataCredentials {

    public static class Steps {


        public static final String STEPOK = "Avanzammos correctamente al paso siguiente.";


    }

    public static class Texts {

        public static final String LINKSNOTFOUND = "No se visualiza el link 'Firmar test de conocimientos y experiencia' buscado en este usuario.";
        public static final String CLICK = "Hacemos click en el botón ";
        public static final String INITSEARCH = "Bucamos el elemento en la lista.";

    }

    public static class Exception {
        public static final String NULLPOINTER = "El elemento llega a null.";
        public static final String NOSUCHELEMENT = "El elemento no se encuentra por pantalla.";
        public static final String TIMEOUTEXCEPTION = "Exceso de tiempo, no se ha podido clicar el elemento.";
        public static final String TIMEEXCEPTIONWAIT = "Se ha cosumido el tiempo de espera del elemento web.";
        public static final String INTERCEPTED = "No se ha podido hacer click sobre el elemento.";

    }


}
