package com.Clicars.Utils;

import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static com.Clicars.Utils.KnowledgeTestProperties.getPropertie;


public class WebDriverSetup {

    private static final Logger LOGGER = LoggerFactory.getLogger(WebDriverSetup.class);

    private static WebDriver driver;

    public static WebDriver getCurrentDriver() {
        if (driver == null) {
            initDriver();
        }
        return driver;
    }

    public WebDriver getDriver(){
        return driver;
    }


    public static void initDriver() {
        LOGGER.debug("Navegador='" + getPropertie("browser") + "'");
        LOGGER.debug("Inicializacion driver='" + getPropertie("initialization") + "'");
        if ("CHROME".equals(getPropertie("browser").toUpperCase())) {
            if ("MANUAL".equalsIgnoreCase(getPropertie("initialization"))) {
                System.setProperty("webdriver.chrome.driver", getPropertie("driversPath") + "chromedriver.exe");
            } else {
                configAutoChrome();
            }
            driver = new ChromeDriver(getChromeOptions());
        }
    }

    private static ChromeOptions getChromeOptions() {
        ChromeOptions chromeOptions = new ChromeOptions();
        chromeOptions.addArguments(
                "--disable-web-security",
                "--ignore-certificate-errors",
                "--allow-running-insecure-content",
                "--allow-insecure-localhost",
                "--no-sandbox"
        );

        return chromeOptions;
    }

    private static void configAutoChrome() {
        LOGGER.info("Version Chrome='" + getPropertie("auto.chrome") + "'");
        LOGGER.info("Arquitectura='" + getPropertie("auto.architecture") + "'");
        WebDriverManager webDriverManager = WebDriverManager.chromedriver();
        configArchitecture(webDriverManager);
        if(null != getPropertie("auto.chrome")) {
            webDriverManager.browserVersion(getPropertie("auto.chrome"));
        }
        webDriverManager.setup();
    }

    private static void configArchitecture(WebDriverManager webDriverManager) {
        if("32".equals(getPropertie("auto.architecture"))) {
            webDriverManager.arch32();
        } else if("64".equals(getPropertie("auto.architecture"))) {
            webDriverManager.arch64();
        }
    }

    public void closeBrowser() {
        if(driver != null) {
            try {
                driver.quit();
                driver = null;
            } catch (Exception ignore) { }
        }
    }

    public Object executeJavascriptCommand(String command) {
        return ((JavascriptExecutor) getCurrentDriver()).executeScript(command);
    }

}
