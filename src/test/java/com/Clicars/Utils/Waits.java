package com.Clicars.Utils;

import org.junit.Assert;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;

import static com.Clicars.Utils.DataCredentials.Exception.TIMEEXCEPTIONWAIT;


public class Waits  {

    public WebDriverWait wait;
    public WebDriverSetup webDriverSetup = new WebDriverSetup();

    private static final Logger LOGGER = LoggerFactory.getLogger(Waits.class);

    public Waits(WebDriverSetup webDriverSetup) {
        wait = new WebDriverWait(webDriverSetup.getDriver(), 60);
    }


    public void waitForPageLoaded(int seg) {
        ExpectedCondition expectation = (ExpectedCondition<Boolean>) driver -> {
            assert driver != null;
            return ((JavascriptExecutor) driver).executeScript("return document.readyState").toString().equals("complete");
        };
        try {
            Thread.sleep(1000L);
            WebDriverWait wait = new WebDriverWait(webDriverSetup.getDriver(), (long) seg);
            wait.until(expectation);
        } catch (Throwable var4) {
            Assert.fail("Timeout waiting for Page Load Request to complete.");
        }
    }

    public WebElement waitElement(WebElement element, int seg) {

        int i = 0;
        WebElement state;
        do {
            state = waits(element);
            if (state == null) {
                wait(1);
                i++;
            } else break;
        } while (i < seg);
        return state;
    }

    public WebElement waits(WebElement element) {

        try {
            element.isDisplayed();
            return element;
        } catch (NoSuchElementException e) {
            return null;
        }
    }

    public WebElement wait(WebElement element, int seg) {

        try {
            return this.wait.until(ExpectedConditions.visibilityOf(element));
        } catch (Exception var4) {
            LOGGER.error(TIMEEXCEPTIONWAIT);
            return null;
        }
    }

    public void waitForElement(List<WebElement> listElement, int j, int seg) {

        int i = 0;
        do {
            try {
                listElement.get(j).isDisplayed();
                break;
            } catch (IndexOutOfBoundsException | NoSuchElementException e) {
                wait(1);
            }
            i++;
            if (i == seg) {
                Assert.fail("El elemento no está por pantalla");
            }
        } while (i < seg&&listElement.get(j) != null);
    }

    public WebElement waitForElementToBePresentAndDisplayed(WebElement element, int seg) {
        WebDriverWait wait = new WebDriverWait(webDriverSetup.getCurrentDriver(), (long) seg);
        return (WebElement) wait.until(ExpectedConditions.visibilityOf(element));
    }

    public void wait(int seg) {
        try {
            Thread.sleep((long) (1000 * seg));
        } catch (InterruptedException var3) {
            Thread.currentThread().interrupt();
        }
    }

}
