package com.Clicars.Utils;

public class AssertMessages {

    public static class Messages {
        public static final String LINKVISSIBLE = "El link 'Firmar test de conocimientos y experiencia' esta visible para este usuario";
        public static final String StepFail = "No hemos avanzado a la pantalla esperada.";
        public static final String SIGNATUREOK = "La operación de firma se ha procesado correctamete.";
        public static final String SIGNATUREKO = "La operación de firma ha fallado.";

    }
}
