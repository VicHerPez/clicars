package com.Clicars.Utils;

import cucumber.api.Scenario;
import org.apache.commons.lang3.StringUtils;
import org.openqa.selenium.*;
import org.openqa.selenium.support.PageFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.Assert;

import java.util.List;
import java.util.NoSuchElementException;

import static com.Clicars.Utils.AssertMessages.Messages.StepFail;
import static com.Clicars.Utils.DataCredentials.Exception.*;
import static com.Clicars.Utils.DataCredentials.Steps.STEPOK;
import static com.Clicars.Utils.DataCredentials.Texts.CLICK;
import static com.Clicars.Utils.KnowledgeTestProperties.getPropertieCar;

public abstract class FunctionsUtils {

    private static final Logger LOGGER = LoggerFactory.getLogger(FunctionsUtils.class);
    public Waits waits;
    public WebDriverSetup webDriverSetup;

    public FunctionsUtils(WebDriverSetup webDriverSetup) {
        this.webDriverSetup = webDriverSetup;
        waits = new Waits(webDriverSetup);
        PageFactory.initElements(this.webDriverSetup.getDriver(), this);

    }


    public void CLICK(WebElement element, int seg) {

        WebElement state = waits.waitElement(element, seg);

        try {
            if (state == null) {
                LOGGER.info("El elemento no se encuentra en la pantalla.");
                Assert.fail("El elemento no se encuentra en la pantalla.");
            } else {
                if (!element.getText().isEmpty()) {
                    LOGGER.info(CLICK + "'" + element.getText() + "'");
                } else {
                    LOGGER.info(CLICK + "'" + element.getAttribute("value") + "'");
                }
                element.click();
            }
        } catch (NullPointerException e) {
            Assert.fail(NULLPOINTER);
        } catch (NoSuchElementException | StaleElementReferenceException f) {
            Assert.fail(NOSUCHELEMENT);
        } catch (TimeoutException t) {
            Assert.fail(TIMEOUTEXCEPTION);
        } catch (ElementClickInterceptedException y) {
            Assert.fail(INTERCEPTED);
        }
    }


    public void verifyStateStep(WebElement element, int seg) {

        WebElement state;
        int i = 0;
        do {
            state = waits.waitElement(element, seg);
            i++;
        } while (state == null && element.getText().isEmpty());
        waits.wait(2);
        if (state != null) {
            LOGGER.info("El texto del elemento de verificación es: " + element.getText());
            LOGGER.info(STEPOK);
        } else {
            LOGGER.info(StepFail);
            LOGGER.error(StepFail);
            Assert.fail(StepFail);
        }

    }


    public static void setLogger(Scenario scenario) {
        LOGGER.info("El resultado del scenario es: " + scenario.getStatus());
    }

    public WebElement carBrandAndModel(String Value) {
        String sValue = getPropertieCar(Value);
        WebElement element;
        do {
            try {
                element = webDriverSetup.getDriver().findElement(By.cssSelector("li>input[value='" + sValue + "']"));
            } catch (NoSuchElementException e) {
                element = null;
            }
            return element;
        } while (element != null);

    }

    public String[] saveDto(List<WebElement> element, int pos, String split) {

        return element.get(pos).getText().split(split);
    }

    public String[] saveData(WebElement element, String split) {

        return element.getText().split(split);
    }

    public String[] textElements(List<WebElement> elementList) {
        int i = 0;
        String[] arrayList = new String[elementList.size()];
        do {
            arrayList[i] = elementList.get(i).getText();
            i++;
        }while (i<elementList.size());

        return arrayList;
    }

    public void compareElement(String[] arrayList, String text, List<WebElement> textsData) {

        boolean state;
        int i = 0;
        do {
            state = org.apache.commons.lang3.StringUtils.containsIgnoreCase(arrayList[i],text);
            if(state){
                LOGGER.info("La caracteristica "+textsData.get(i).getText()+" es correcta.");
            }
            i++;
            if(i==arrayList.length&&!state){
                Assert.fail("La caracteristica "+textsData.get(i).getText()+" es incorrecta.");
            }
        }while (!state&&i<arrayList.length);

    }

    public void compareElementText(String[] arrayList, String text, String textsData) {

        boolean state;
        int i = 0;
        do {
            state = org.apache.commons.lang3.StringUtils.containsIgnoreCase(arrayList[i],text);
            if(state){
                LOGGER.info("La caracteristica "+textsData+" es correcta.");
            }
            i++;
            if(i==arrayList.length&&!state){
                Assert.fail("La caracteristica "+textsData+" es incorrecta.");
            }
        }while (!state&&i<arrayList.length);

    }

}


