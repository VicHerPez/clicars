package com.Clicars.Utils;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class KnowledgeTestProperties extends FunctionsUtils {

    private static final Logger LOGGER = LoggerFactory.getLogger(KnowledgeTestProperties.class);
    public static Properties props = null;
    public static Properties propsCar = null;

    public KnowledgeTestProperties(WebDriverSetup webDriverSetup) {
        super(webDriverSetup);
    }


    public static Properties GetConfig(String s) {
        if (props == null) {
            props = initializeProperties(s + ".properties");
        }
        return props;

    }

    public static Properties GetConfigCar(String s) {
        if (propsCar == null) {
            propsCar = initializeProperties(s + ".properties");
        }
        return propsCar;

    }

    private static Properties initializeProperties(String fileName) {
        Properties prop = new Properties();
        InputStream input;
        try {
            input = new FileInputStream("src/test/resources/" + fileName);
            // load a properties file
            prop.load(input);
        } catch (IOException ex) {
            LOGGER.error("Loading properties file '" + fileName + "' failed, check configuration file location");
        }
        return prop;
    }

    public static String getPropertie(String questions) {
        if (System.getProperty(questions) == null) {
            return props.getProperty(questions);

        }
        return System.getProperty(questions);
    }

    public static String getPropertieCar(String questions) {
        if (System.getProperty(questions) == null) {
            return propsCar.getProperty(questions);

        }
        return System.getProperty(questions);
    }

}
