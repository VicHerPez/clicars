package com.Clicars;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;
import io.cucumber.testng.AbstractTestNGCucumberTests;
import org.junit.runner.RunWith;

@RunWith(Cucumber.class)
@CucumberOptions(
        strict = true,
        features = "src/test/resources/cucumberFeatures/",
        plugin = {"json:target/cucumber-report/cucumber.json","pretty"},
        monochrome = false,
        tags = "@ignore"
)
public class TestRunner extends AbstractTestNGCucumberTests {
}
