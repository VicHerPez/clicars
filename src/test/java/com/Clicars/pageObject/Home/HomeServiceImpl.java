package com.Clicars.pageObject.Home;

import com.Clicars.Utils.FunctionsUtils;
import com.Clicars.Utils.Waits;
import com.Clicars.Utils.WebDriverSetup;
import org.openqa.selenium.WebElement;

public class HomeServiceImpl extends FunctionsUtils {

    public HomePage homePage = new HomePage(webDriverSetup);
    public Waits waits = new Waits(webDriverSetup);


    public HomeServiceImpl(WebDriverSetup webDriverSetup) {
        super(webDriverSetup);
    }

    public void acceptCookies() {

        WebElement element = waits.waitElement(homePage.getTitleCookies(), 40);
        if (element != null) {
            CLICK(homePage.getButtAcceptCookies(),60);
        }
    }


    public void selectButtFind() {
        CLICK(homePage.getButtFindYourCar(),60);
        waits.waitForPageLoaded(60);
    }


}
