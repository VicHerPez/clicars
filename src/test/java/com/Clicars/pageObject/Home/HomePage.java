package com.Clicars.pageObject.Home;

import com.Clicars.Utils.FunctionsUtils;
import com.Clicars.Utils.WebDriverSetup;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class HomePage extends FunctionsUtils {

    public HomePage(WebDriverSetup webDriverSetup) {
        super(webDriverSetup);
    }

    @FindBy(css = "div>a[class=\"bannerBtn____IhR- default___1FRAY\"]")
    private WebElement buttFindYourCar;

    @FindBy(css = "button[class=\"button___2R6qU size-sm___3TKQS default___1FRAY\"]")
    private WebElement buttAcceptCookies;

    @FindBy(css = "div>h1[class=\"title___2DV4T underline___dbvD3 variant-h3___ANSPB color-secondary___3vq1b weight-normal___25n2H align-center___17w2J\"]")
    private WebElement titleCookies;



    public WebElement getButtFindYourCar() {
        return buttFindYourCar;
    }

    public WebElement getButtAcceptCookies() {
        return buttAcceptCookies;
    }

    public WebElement getTitleCookies() {
        return titleCookies;
    }
}
