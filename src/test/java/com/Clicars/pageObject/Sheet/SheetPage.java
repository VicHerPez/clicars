package com.Clicars.pageObject.Sheet;

import com.Clicars.Utils.FunctionsUtils;
import com.Clicars.Utils.WebDriverSetup;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import java.util.List;
import java.util.NoSuchElementException;

public class SheetPage extends FunctionsUtils {

    public SheetPage(WebDriverSetup webDriverSetup) {
        super(webDriverSetup);
    }

    @FindBy(css = "div>h1[data-qa-selector=\"vehicle-info-title\"]")
    private WebElement textBrand;

    @FindBy(css = "div>h2[data-qa-selector='vehicle-info-subtitle']")
    private WebElement textModel;

    @FindBy(css = "div>p[data-qa-selector='vehicle-info-price']")
    private WebElement textPrice;

    @FindBy(css = "p[data-qa-selector=\"vehicle-info-financed-price\"]")
    private WebElement priceFinanced;

    @FindBy(css = "div>span[class='listItemValue___1IWSE']")
    private List<WebElement> elementListDataBasic;

    @FindBy(css = "div>h5")
    private List<WebElement> textDataCar;

    @FindBy(css = "div>button[data-qa-selector=\"buy-now\"]")
    private WebElement buttBuyNow;

    //GETTER WEBELEMENT
    public List<WebElement> getTextDataCar() {
        return textDataCar;
    }

    public WebElement getTextBrand() {
        return textBrand;
    }

    public WebElement getTextModel() {
        return textModel;
    }

    public WebElement getPriceFinanced() {
        try{
            priceFinanced.isDisplayed();
        }catch (Exception e){
            priceFinanced = null;
        }
        return priceFinanced;
    }

    public WebElement getTextPrice() {
        return textPrice;
    }

    public List<WebElement> getElementListDataBasic() {
        return elementListDataBasic;
    }

    public WebElement getButtBuyNow() {
        return buttBuyNow;
    }
}
