package com.Clicars.pageObject.Sheet;

import com.Clicars.Utils.FunctionsUtils;
import com.Clicars.Utils.Waits;
import com.Clicars.Utils.WebDriverSetup;
import org.junit.Assert;

import static com.Clicars.Dto.Dto.*;
import static com.google.common.base.Ascii.toLowerCase;

public class SheetServiceImpl extends FunctionsUtils {

    public SheetPage sheetPage = new SheetPage(webDriverSetup);
    public Waits waits = new Waits(webDriverSetup);


    public SheetServiceImpl(WebDriverSetup webDriverSetup) {
        super(webDriverSetup);
    }

    public void verifyElements(){
        Assert.assertEquals("No coincide la marca de coche de la ficha con la seleccion en la pantalla de búsqueda",sheetPage.getTextBrand().getText(),getBrand());
        Assert.assertEquals("No coincide el modelo de coche de la ficha con la seleccion en la pantalla de búsqueda",sheetPage.getTextModel().getText(),getModel());
        if (sheetPage.getPriceFinanced()==null){
            Assert.assertEquals("No coincide el precio de coche de la ficha con la seleccion en la pantalla de búsqueda",sheetPage.getTextPrice().getText(),getPrice());
        } else {
            Assert.assertEquals("No coincide el precio de coche de la ficha con la seleccion en la pantalla de búsqueda",sheetPage.getPriceFinanced().getText(),getPrice());
            setPrice(sheetPage.getTextPrice().getText());
        }

        String[] dataCar = textElements(sheetPage.getElementListDataBasic());
        compareElement(dataCar,getFuel(),sheetPage.getTextDataCar());
        compareElement(dataCar,getKm(),sheetPage.getTextDataCar());
        compareElement(dataCar,getYear(),sheetPage.getTextDataCar());
        compareElement(dataCar,getTransmission(),sheetPage.getTextDataCar());

    }

    public void BuyNow(){
        CLICK(sheetPage.getButtBuyNow(),60);
        waits.waitForPageLoaded(60);
    }


}
