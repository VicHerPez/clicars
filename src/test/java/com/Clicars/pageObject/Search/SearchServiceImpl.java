package com.Clicars.pageObject.Search;

import com.Clicars.Utils.FunctionsUtils;
import com.Clicars.Utils.Waits;
import com.Clicars.Utils.WebDriverSetup;

import static com.Clicars.Dto.Dto.*;
import static com.Clicars.Utils.KnowledgeTestProperties.getPropertieCar;

public class SearchServiceImpl extends FunctionsUtils {

    public SearchPage searchPage = new SearchPage(webDriverSetup);
    public Waits waits = new Waits(webDriverSetup);
    public static String[] data;


    public SearchServiceImpl(WebDriverSetup webDriverSetup) {
        super(webDriverSetup);
    }

    public void selectBrand() {
        verifyStateStep(searchPage.getTitlePageSearch(),60);
        CLICK(searchPage.getButtBrand(),60);
        waits.waitElement(searchPage.getElement1Verify(),60);
        CLICK(carBrandAndModel("Brand"),60);
        waits.waitElement(searchPage.getElementVerify(),50);
        waits.wait(1);
        CLICK(carBrandAndModel("Model"),60);
        waits.waitForPageLoaded(60);
    }

    public void selectCar(){
        waits.waitElement(searchPage.getListCar().get((Integer.parseInt(getPropertieCar("Pos")))+1),60);
        data = saveDto(searchPage.getListCar(), (Integer.parseInt(getPropertieCar("Pos")))+1,"\n");
        setBrand(data[0]);
        setPrice(data[1]);
        setModel(data[2]);
        setFuel(data[6]);
        setKm(data[8]);
        setTransmission(data[9]);
        setYear(data[4]);
        CLICK(searchPage.getListCar().get((Integer.parseInt(getPropertieCar("Pos")))+1),60);
        waits.waitForPageLoaded(60);

    }


}
