package com.Clicars.pageObject.Search;

import com.Clicars.Utils.FunctionsUtils;
import com.Clicars.Utils.WebDriverSetup;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import java.util.List;

public class SearchPage extends FunctionsUtils {

    public SearchPage(WebDriverSetup webDriverSetup) {
        super(webDriverSetup);
    }

    @FindBy(css = "h1[class= \"title___1YEGU\"]")
    private WebElement titlePageSearch;

    @FindBy(id = "carMakeFilter")
    private WebElement buttBrand;

    @FindBy(css = "#app > div > main > div > div.filterSectionWrapper___SrkFt > div > div.filters___1lGDM > div:nth-child(2) > div > div.container___2FB5N > div:nth-child(1) > h3")
    private WebElement element1Verify;

    @FindBy(css = "div>button[class=\"buttonBack___32t8Z\"]")
    private WebElement elementVerify;

    @FindBy(css = "div[class=\"ReactVirtualized__Grid__innerScrollContainer\"]>div[style]")
    private List<WebElement> listCar;


    //GETTER WEBELEMENT
    public WebElement getButtBrand() {
        return buttBrand;
    }

    public WebElement getTitlePageSearch() {
        return titlePageSearch;
    }

    public WebElement getElement1Verify() {
        return element1Verify;
    }

    public WebElement getElementVerify() {
        return elementVerify;
    }

    public List<WebElement> getListCar() {
        return listCar;
    }
}
