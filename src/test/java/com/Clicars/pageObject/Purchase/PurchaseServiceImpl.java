package com.Clicars.pageObject.Purchase;

import com.Clicars.Utils.FunctionsUtils;
import com.Clicars.Utils.Waits;
import com.Clicars.Utils.WebDriverSetup;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.Assert;

import static com.Clicars.Dto.Dto.*;
import static com.google.common.base.Ascii.toLowerCase;

public class PurchaseServiceImpl extends FunctionsUtils {

    public PurchasePage purchasePage = new PurchasePage(webDriverSetup);

    private static final Logger LOGGER = LoggerFactory.getLogger(FunctionsUtils.class);

    public PurchaseServiceImpl(WebDriverSetup webDriverSetup) {
        super(webDriverSetup);
    }

    public void verifyPurchase(){
        verifyStateStep(purchasePage.getTitlePurchase(),60);
        String[] ModelBrand = saveData(purchasePage.getTextBrandModel(),"\n");
        compareElementText(ModelBrand,getYear(),"Año");
        compareElementText(ModelBrand,getBrand(),"Marca");
        String[]Characteristic = saveData(purchasePage.getTextsCharacteristic(),"\\|");
        compareElementText(Characteristic,getKm(),"Kilometraje");
        compareElementText(Characteristic,getFuel(),"Carburante");
        compareElementText(Characteristic,getTransmission(),"manual");
        if (purchasePage.getPrice().getText().equals(getPrice())){
            LOGGER.info("La caracteristica Precio es correcta.");
        } else {
            Assert.fail("El precio no coincide");
        }



    }


}
