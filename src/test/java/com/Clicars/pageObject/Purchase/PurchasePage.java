package com.Clicars.pageObject.Purchase;

import com.Clicars.Utils.FunctionsUtils;
import com.Clicars.Utils.WebDriverSetup;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import java.util.List;

public class PurchasePage extends FunctionsUtils {

    public PurchasePage(WebDriverSetup webDriverSetup) {
        super(webDriverSetup);
    }

    @FindBy(css = "form>h1[class=\"title___2DV4T underline___dbvD3 variant-h3___ANSPB color-secondary___3vq1b weight-normal___25n2H align-center___17w2J\"]")
    private WebElement titlePurchase;

    @FindBy(css = "a[data-qa-selector=\"car-title\"]")
    private WebElement textBrandModel;

    @FindBy(css = "section[data-qa-selector=\"section\"]>div>div[class=\"subTitle___3bI29\"]")
    private WebElement textsCharacteristic;

    @FindBy(css = "span[data-qa-selector=\"price-item-undefined-price\"]")
    private WebElement price;

    //GETTER WEBELEMENT
    public WebElement getTitlePurchase() {
        return titlePurchase;
    }

    public WebElement getTextBrandModel() {
        return textBrandModel;
    }

    public WebElement getTextsCharacteristic() {
        return textsCharacteristic;
    }

    public WebElement getPrice() {
        return price;
    }
}
