package com.Clicars.Steps;

import com.Clicars.Utils.WebDriverSetup;
import com.Clicars.pageObject.Home.HomeServiceImpl;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;

public class HomeSteps {

    public HomeServiceImpl homeService;

    public HomeSteps(WebDriverSetup webDriverSetup) {
        homeService = new HomeServiceImpl(webDriverSetup);
    }

    @Given("^I clic in button of find$")
    public void iClicInButtonOfFind() {
        homeService.selectButtFind();
    }

}
