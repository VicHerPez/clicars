package com.Clicars.Steps;


import com.Clicars.Utils.FunctionsUtils;
import com.Clicars.Utils.Waits;
import com.Clicars.Utils.WebDriverSetup;
import com.Clicars.pageObject.Home.HomeServiceImpl;
import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java.en.Given;
import org.openqa.selenium.JavascriptExecutor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static com.Clicars.Utils.FunctionsUtils.setLogger;
import static com.Clicars.Utils.KnowledgeTestProperties.*;


public class HookStep {

    private static final Logger LOGGER = LoggerFactory.getLogger(HookStep.class);

    private String URL;
    private String language;
    public WebDriverSetup webDriverSetup = new WebDriverSetup();
    private Waits waits;
    private HomeServiceImpl homeService;
    public FunctionsUtils functionsUtils;

    @Before
    public void openBrowser(Scenario scenario) {
        GetConfig("Setup");
        GetConfigCar("configCar");
        URL = getPropertie("URL");
        language = getPropertie("language");
        LOGGER.info("SCENARIO: " + scenario.getName());
        try {
            webDriverSetup.getCurrentDriver().manage().window().maximize();
        } catch (RuntimeException ignore) {
            ignore.getCause();
        }
        waits = new Waits(webDriverSetup);
        homeService = new HomeServiceImpl(webDriverSetup);
    }

    @Given("^I navegate to URL$")
    public void iNavegateToPageAutohero() {
        navigateURL();
        waits.waitForPageLoaded(60);
        homeService.acceptCookies();
    }

    @After
    public void embedScreenshot(Scenario scenario) {

        try {
            Thread.sleep(2000);
            LOGGER.info("END SCENARIO: " + scenario.getName());
            setLogger(scenario);
        } catch (InterruptedException e) {
            LOGGER.error(e.getMessage());
        }
//        webDriverSetup.closeBrowser();
    }

    public String selectLanguage(){

        String languageCod = null;
        if(language.equalsIgnoreCase("Español")){
            languageCod = "es";
        } else if (language.equalsIgnoreCase("Inglés")){
            languageCod = "es";
        }
        return languageCod;
    }

    public void navigateURL(){
        LOGGER.info("Navegamos a la URL: "+URL+selectLanguage());
        ((JavascriptExecutor) webDriverSetup.getCurrentDriver()).executeScript("this.document.location = " + "\'" + (URL+selectLanguage()) + "'");
    }

}
