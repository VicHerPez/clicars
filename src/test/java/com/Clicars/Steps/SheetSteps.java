package com.Clicars.Steps;

import com.Clicars.pageObject.Sheet.SheetServiceImpl;
import cucumber.api.java.en.And;

public class SheetSteps {

    public SheetServiceImpl sheetService;

    public SheetSteps(SheetServiceImpl sheetService){
        this.sheetService = sheetService;
    }


    @And("^I verify the data of the selected car$")
    public void iVerifyTheDataOfTheSelectedCar() {
        sheetService.verifyElements();
    }

    @And("^I clic in button BuyNow$")
    public void iClicInButtonBuyNow() {
        sheetService.BuyNow();
    }
}
