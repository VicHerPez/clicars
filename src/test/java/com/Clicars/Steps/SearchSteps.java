package com.Clicars.Steps;

import com.Clicars.Utils.WebDriverSetup;
import com.Clicars.pageObject.Search.SearchServiceImpl;
import cucumber.api.java.en.And;
import cucumber.api.java.en.When;

public class SearchSteps {

    public SearchServiceImpl searchService;

    public SearchSteps(WebDriverSetup webDriverSetup){
        searchService = new SearchServiceImpl(webDriverSetup);
    }

    @When("^I select the brand$")
    public void iSelectTheBrand() {
        searchService.selectBrand();
    }

    @And("^I select the Car$")
    public void iSelectTheCar() {
        searchService.selectCar();
    }
}
