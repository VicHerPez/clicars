package com.Clicars.Steps;

import com.Clicars.pageObject.Purchase.PurchaseServiceImpl;
import com.Clicars.pageObject.Sheet.SheetServiceImpl;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;

public class PurchaseSteps {

    public PurchaseServiceImpl purchaseService;

    public PurchaseSteps(PurchaseServiceImpl purchaseService){
        this.purchaseService = purchaseService;
    }

    @Then("^I verify page Purchase$")
    public void iVerifyPagePurchase() {
        purchaseService.verifyPurchase();
    }


}
