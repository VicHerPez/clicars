Feature: Criterios de aceptación página https://kinepolis.es/peliculas/el-ultimo-duelo

  Scenario: Información preliminar
    Given Un usuario que ha accedido a la página 'https://kinepolis.es'
    When Ha buscado por la película 'el ultimo duelo'
    Then Visualizará información básica de la película, una breve sinopsis y los actores principales

  Scenario: Visualización de sesiones
    Given  Un usuario que ha accedido a la página 'https://kinepolis.es/peliculas/el-ultimo-duelo'
    When Ha informado la fecha y lugar de la pelicula
    Then Visualizara las distintas sesiones

  Scenario: Pantalla precios
    Given Un usuario que ha accedido a la página 'https://kinepolis.es/peliculas/el-ultimo-duelo'
    When El usuario ha seleccionado la sesión que desea
    Then Visualizará los diferentes precios disponibles y un selector de numero de entradas

  Scenario: Valor total de entradas
    Given Un usuario que ha accedido a la página 'https://kinepolis.es/peliculas/el-ultimo-duelo'
    When El usuario ha seleccionado el precio de la entrada y el número de entradas
    Then Se visualizara el valor total de las entradas seleccionadas

  Scenario: Selección de butaca correctamente
    Given Un usuario que ha accedido a la página 'https://kinepolis.es/peliculas/el-ultimo-duelo'
    When El usuario ha seleccionado el tipo de entrada y el numero de entradas
    Then Visualizaremos la pantalla de selección de butacas con las butacas disponibles

  Scenario Outline: Selección de butaca erróneamente
    Given Un usuario que ha comprado una entrada normal
    When El usuario intenta seleccionar una butaca "<Butaca>"
    Then No se puede seleccionar una butaca que no está disponible

    Examples: Butacas
      | Butaca           |
      | No disponible    |
      | Sillas de ruedas |
      | VIP              |

    Scenario: Formulario pago
      Given Un usuario que ha seleccionado las butacas correctamente
      When Rellena el formulario de pago correctamente
      Then Se visualizara una landing de confirmación

  Scenario: Formulario pago superior a 5 minutos
    Given Un usuario que ha seleccionado las butacas correctamente
    When Tarda mas de 5 minutos en rellenar el formuladrio
    Then No se completará el metodo de pago

  Scenario: Formulario pago
    Given Un usuario que ha seleccionado las butacas correctamente
    When Rellena el formulario de pago correctamente antes de 5 minutos
    Then El usuario recibe un email con el resumen de la compra y las entradas